# T2K Flux Instructions

This document is a set of instructions for producing a flux using FLUKA and Jnubeam. Specifically the instructions are for producing the tuned flux release as an input to the oscillation analysis.
