\documentclass[12pt]{article}
\usepackage[margin=1.00in]{geometry}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{hyperref}

\begin{document}

    \title{The Formula to Fabricate Flux Files}
    \author{A. B. Cudd, L. Zambelli}
    \maketitle

    \section{Details of the Machinery}
        A description of all the flux and reweighting machinery can be found in the published paper \cite{flux_paper} and various technical notes \cite{TN38, TN39, TN99, TN217, TN264}.

    \section{Setting up the Code}
        \subsection{Terminology}
            This guide is meant to be a document on how to install and run the T2K flux generation on a UNIX-like system, using the BASH shell or something equivalent. Any code will assume a BASH shell, and modifications may be necessary to use another shell, such as CSH or ZSH. Next since in principle it does not matter where the code is installed (as long as it's on the KEKCC), I will use the following convention for specifying directories:
            \begin{verbatim} /path/to/directory \end{verbatim}
            where the \verb!/path/to/! represents whatever path is necessary to reach \verb!directory/! since I can not assume I know where you installed the various code. For example, if you installed the code into your HOME directory then:
            \begin{verbatim} /path/to/directory = /home/t2k/username/directory \end{verbatim}
            where \verb!username! is your KEKCC username. If you installed the code into say a \verb!flux/! directory in your HOME directory then:
            \begin{verbatim} /path/to/directory = /home/t2k/username/flux/directory \end{verbatim}
            and so on and so forth for any directory you can think of. This allows for easy distinction between paths you need to specify and paths that must be entered verbatim.

        \subsection{KEKCC Account}
            To run all the flux generation code you need to have a KEKCC account. If you do not have one, the instructions for obtaining a KEKCC accound are available on t2k.org, located here: \url{http://www.t2k.org/local/kek/kekcc/get_account} It involves filling out a couple forms and then emailing those forms to the correct person. Once you are logged on to KEKCC, there are a few environment variables that should be set for the flux code. \\

            These can be put into your \verb!.bashrc! or equivalent, or in a separate setup script for flux generation. The paths for GCALOR and CERNLIB may need to be changed depending on the compilier used.
            \begin{verbatim}
export GCALOR=/group/t2k/beam/mc/beamMC/gcalor/gcalor.o
export CERN_ROOT=/group/t2k/beam/mc/beamMC/cernlib/2005
export CERN=/group/t2k/beam/mc/beamMC/cernlib/2005
export CERN_LEVEL=2005
export ROOTDIR=/group/t2k/beam/mc/beamMC/root_5.34/root
source ${ROOTDIR}/bin/thisroot.sh
module load gcc/530
            \end{verbatim}

        \subsection{Installing Jnubeam}
            Download the most recent version of Jnubeam from the CVS repository using the following settings, followed by the checkout command:
            \begin{verbatim}
export CVSROOT=:ext:jnudevel@jnusrv01.kek.jp:/home/jnurep/repository
export CVS_RSH=ssh
export CVS_SERVER="env LOGNAME=jnudevel cvs"
cvs co jnubeam
            \end{verbatim}
            The CVS repository is password protected. Officially you need to ask one of the conveners: Mark Hartz, T. Nakadaira, or A. Ichikawa. However other people working on flux generation probably also know the password. If you have a CVS account on jnusrv, you can use it instead of the generic jnudevel account. \\

            Jnubeam can be compiled with either gfortran or g77 for the flux production. The correct version of the the GCALOR and CERNLIB libraries must be loaded to match the compiler for Jnubeam, otherwise the compilation will fail. There are also some slight Makefile differences between the two compiliers.

            \subsubsection{Compiling with gfortran}

                The gfortran versions of the required libraries can be found here:
                \begin{verbatim}
GCALOR = /group/t2k/beam/mc/beamMC/docs/flux_production/gcalor/gfortran/gcalor.o
CERN_ROOT = /group/t2k/beam/mc/beamMC/cernlib/2005
CERN = /group/t2k/beam/mc/beamMC/cernlib/2005
                \end{verbatim}

                The KEKCC default compiler is too old to have the proper version of gfortran necessary; use \verb!module load gcc/530! (or gcc/485) to load a newer compiler. The \verb!Makefile.env! needs to be updated to use \verb!gfortran! by setting \verb!FC=gfortran! and removing the \verb!-fno-globals! and \verb!-Wno-globals! flags from \verb!FFLAGS!. \\

                In \verb!jnubeam/libnubeam/jhorn3.F! file, change the \verb!#define ROO RO+OTHICK! line (around line 14) to the following:
                \begin{verbatim}
C ROO = RO + OTHICK
#define ROO 66.5
                \end{verbatim}

                The other change is a subtle one; gfortran and g77 treat tab characters differently when compiling. This causes an error originating from the \verb!jnubean/nubeam/uhdef.ch! file. The following command will replace all tab characters in the file with spaces:
                \begin{verbatim}
expand -t 4 uhdef.ch > tmp.ch && mv tmp.ch uhdef.ch
                \end{verbatim}

                If the expand command is not available, the same effect can be achieved with sed:
                \begin{verbatim}
sed -i 's/\t/    /g' uhdef.ch 
                \end{verbatim}

                Those should be all the changes necessary to compile with gfortran. In the \verb!jnubeam/! directory say a prayer and run \verb!make! to attempt to compile the code. If (or when) the compilation fails, try contacting someone who might be helpful and hope. \\

            \subsubsection{Compiling with g77}

                The g77 versions of the required libraries can be found here:
                \begin{verbatim}
GCALOR = /group/t2k/beam/mc/beamMC/docs/flux_production/gcalor/g77/gcalor.o
CERN_ROOT = /gpfs/home/t2k/mhartz/cern/jbuild/2005
CERN = /gpfs/home/t2k/mhartz/cern/jbuild/2005
                \end{verbatim}

                Jnubeam was originally written to be compiled with g77, and there are no changes that are necessary to compile. The only requirement is that the correct g77 versions of the libraries are found. \\

                In the \verb!jnubeam/! directory say a prayer and run \verb!make! to attempt to compile the code. If (or when) the compilation fails, try contacting someone who might be helpful and hope. \\
        
        \subsection{Installing FLUKA}
            Download the code for running FLUKA in the same way as Jnubeam (using the same password), but with a slightly different checkout command:
            \begin{verbatim}
export CVSROOT=:ext:jnudevel@jnusrv01.kek.jp:/home/jnurep/repository
export CVS_RSH=ssh
export CVS_SERVER="env LOGNAME=jnudevel cvs"
cvs co hadpro
            \end{verbatim}
            There will be multiple directories downloaded, but the important part for generating the flux (running FLUKA and Jnubeam) is the directory \verb!hadpro/fluka_hadint! In order to compile and run FLUKA, the following environment variables need to be set:
            \begin{verbatim}
export PATH=/group/t2k/beam/mc/beamMC/usr/bin:$PATH
export LD_LIBRARY_PATH=/path/to/hadpro/fluka_hadint:$LD_LIBRARY_PATH
export ROOTDIR=/group/t2k/beam/mc/beamMC/root_5.34/root
source ${ROOTDIR}/bin/thisroot.sh
export FLUKABIN=/path/to/hadpro/fluka_hadint
export FLUPRO=/group/t2k/beam/mc/beamMC/fluka2011.2c6
export FLUFOR=gfortran
module load gcc/530
            \end{verbatim}
            There is a \verb!set_env_fluka.sh! script to set these variables (check if it is up to date with the proper directories). Run \verb!make! inside \verb!/fluka_hadint/! to compile the FLUKA library and executable. Unlike Jnubeam, FLUKA requires gfortran to compile. The FLUPRO and FLUKA version quoted here is from August 2017, and whenever a new version of FLUKA is released, the old versions cease to work and the paths must be updated.
        
    \section{Generating Flux Files}
        Generating the flux files is done in several, distinct steps: running FLUKA, running Jnubeam, and then running the tuning code.

        \subsection{Running FLUKA}
            The first step in running FLUKA is making sure all the necessary environment variables have been set (this should be done in the submission script mentioned below). These are the same environment variables used when compiling the FLUKA executable. To produce flux files FLUKA requires an input card file, denoted by \verb!card_file_name.inp! which contains all the beam and simulation parameters. There should be an example FLUKA card file which contains the previous beam parameters as a guide for flux generation. \\

            The beam parameters for a given run will be both in various slides / presentations about the beam conditions and in the flux technote, TN-217. For standard flux generation the only parameters that need to be changed are the beam position and emittance parameters: 
            \begin{verbatim} x, y, x', y', wx, wy, emit_x, emit_y, alpha_x, alpha_y \end{verbatim}

            Carefully check the units required by the card file and the units given in the slides / documentation as they may not match. For later use in Jnubeam, the x and x' parameters MUST be multiplied by -1 to be correct. Also be sure to comment the new parameters in the card file. \\

            There should be an example submission script for running all the FLUKA jobs; if not, one can be found here: \verb!/group/t2k/beam/mc/beamMC/docs/flux_production/scripts!. Edit this script for your production run, setting the proper card file, output directories, file naming, and the RAND seed to be used. The RAND seed can be any number, usually some five digit number. The suggested output directory for all the files is the Beam group workdisk (\verb!/group/t2k/beam/work/!) so create a user directory there if you do not already have one. Once the submission script has been properly edited, simply run the script or submit it to the job queue. \\
            
            Running FLUKA should only take a few hours at most, and once it is finished check the output ROOT and log files. All the ROOT files should be in a single directory and properly named. One quick check to see if any files are bad is to check the size of the ROOT file; all the ROOT files should be about 194 MB (using the default settings in the cardfile). Check the log file for any suspicious runs. If any given run crashed, either rerun that particular run or delete the bad ROOT file. If any bad ROOT files are present, it can cause crashes in later steps. \\

            Check the appendix for a few documented errors when running FLUKA.

        \subsection{Running Jnubeam}
            For flux generation the working directory is the \verb!/jnubeam/nubin! directory. Jnubeam also requires a handful of environment variables to run properly, which should be set in the run scripts. There are also two special data files required for Jnubeam to run: \verb!xsneut.dat! and \verb!chetc.dat! which have to be present in the directory for running. They either should already be in the directory or can be found here \verb!/group/t2k/beam/mc/beamMC/dat_files!. \\
            
            Jnubeam requires an input card file in the same way as FLUKA, however it has a different naming convention: \verb!card_file_name.card! The standard card file for flux generation should be in the directory and only needs to be updated with the horn current for a given run. All three horn currents are needed which can be found in the BSD slides or by asking the right person. If the current is given by CT, the translation is CT4 for Horn 1 and CT5 for Horn 2 and 3. \\ 

            There are two scripts for running Jnubeam and submitting jobs to the queue. The script \verb!sub_jnubeam_jobs.pl! is a Perl script (sorry) that takes the start and end run numbers, which are the random seeds used when generating FLUKA files, and submits jobs to the queue. The script \verb!run_jnubeam.sh! sets up the job and executes the Jnubeam code. Example job scripts and card files can be found here: \\ \verb!/group/t2k/beam/mc/beamMC/docs/flux_production/scripts!

            Each of the scripts contain a small section at the beginning for user settings for a given flux production run. Edit these for your flux production and double check the path and file names. Once the scripts have been setup properly, start the process by running \verb!./sub_jnubeam_jobs.pl [StartRun] [EndRun]! which will submit all the jobs to the batch queue in groups, submitting new jobs when the current jobs finish. It is suggested to run it as a background process, additionally with \verb!nohup! or with \verb!screen!, as the Jnubeam jobs take several hours for each job (this step for the usual 1000+ files will take several days). \\

            Once all the jobs are finished a set of HBK, ROOT, and LOG files should have been produced for each run. Check for any runs that crashed and either rerun them or delete them; it is expected that a small number of jobs will fail. Broken Jnubeam files will later cause the tuning code to break. With the default settings the ROOT files should be about 40 to 45 MB in size. Broken Jnubeam files can commonly be identified by their small size, and a quick way to delete these files is with the following command:
            \begin{verbatim}
find /path/to/jnufiles -name "*.root" -size -30M -delete
            \end{verbatim}

        \subsection{Running Tuning}
            The tuning process is relatively simple; the flux tuning code is located in \verb!jnubeam/ana/flux_tuning! which should have been checked out with Jnubeam. To compile the tuning code run: \verb!make clean; make links; make! which will perform all the compilation. The tuning code is run in two configurations: coarse binning and fine binning. To switch between binnings currenly requires editing the source file. In the \verb!src/tunedet_2014.cc! file, edit the \verb!#define BINTYPE! line to select the number of bins. Coarse binning is one that says 39 bins, but it's actually 59 bins. Once the source file has been changed, compile as before. \\

            The tuning code also requires some environment variables to be set, and there should be a \verb!set_env_tune.sh! script to do that. Running the tuning code is performed by the following command: 
            \begin{verbatim}
./bin/tunedet_2014 -i /path/to/jnubeam_files -o /path/to/output_file.root 
            \end{verbatim}
            Be sure the directory where the output file is saved exists! The input is just the directory itself, not file names. Once the tuning has been run for both fine and coarse binning (which should only take a couple hours), it's time to create the flux release.

    \section{Creating the Release}
        The last major process is packaging the now tuned flux files into the Flux Release, and making plots.

        \subsection{Getting the Code}
            The complete code for making the flux release currently does not come with the checked out Jnubeam code, so you'll have to copy it from somewhere else. Ask someone who previously did the flux relase or check \verb!/group/t2k/beam/mc/beamMC/docs/flux_production/!

        \subsection{Making the Release File}
            Once you have the release code from somewhere, the first step is to compile the code. In the \verb!make_flux_files/! directory run \verb!make links; make! to compile the release code. Next, move the two tuned flux result files to the \verb!make_flux_files/fluxFiles/13a/runXX! directory, where you may have to create a new directory for your flux run. For ease of use later, rename your tuned flux files to match the other runs (name should be \verb!runXX_tune_finebins.root! and \verb!runXX_tune_39bins.root!). Finally create a new directory (if necessary) for the combined flux run, e.g. \verb!fluxFiles/13a/run1-XX! which will be needed later. At the moment, the code relies on this naming scheme even though the coarse binning is actually 59 bins. \\

            In the \verb!make_flux_files/macros/! directory is a set of ROOT macros to combine the flux runs. I would suggest making a copy of the most recent \verb!combine_flux_nu_run1-XX.C! macro to edit for your flux production. The macro needs the following to be updated: the ND280 and SK POT for the new flux run, the number of runs, the new run needs to be added, and the run naming needs to be changed. These edits hopefully should be intuitive, but ask questions if necessary. Once the edits have been made, run the ROOT macro to produce the new combined flux runs. \\

            The next step is to create the raw release files from both the single run and combined run files. First check \verb!src/flux_weight_file.cc! to set the proper variables for neutrino or anti-neutrino mode, namely the input files and output file names. Then run \verb!make clean; make links; make! to compile the code. Next edit the script \verb!sub_release.sh! to select the flux runs and configuration you need, and then run the script. You may need to set the \verb!LD_LIBRARY_PATH! variable to include \verb!make_flux_files/lib/! if the script does not do that already. Once the script finishes running, the raw release files have been created. \\

            The last step is renaming and moving the raw release files around to create the finalized flux release. Create a new directory (if necessary) for the new files in \verb!releaseFiles/tuned13av1.1/! like before. Inside \verb!macros/! there is a script and config file to rename and move the files.  Currently the config file is named \verb!justdoit.txt! which contains a line for each run combination; you can just copy the appropriate lines and edit them. Once the config file is set up, run it with \verb!root -l justdoit.txt! which will iterate through all the commands in the config file. Finally check the final release files for any obvious errors or mistakes. \\

            Once the release files have been determined satisfactory, tar up the \verb!tuned13av1.1! directory with an appropriate name. Before the flux release is sent out to the collaboration it must be presented to the Beam MC group for validation.

        \subsection{Comparing Runs}
            Also included in the code is an executable to compare two specified flux runs, creating ratio plots, etc. Inside the \verb!macros/! directory run \verb!make clean; make! to compile the code. The executable is \verb!macros/bin/compareRuns! which requires a number of command line arguments to run. The arguments are:
            \begin{verbatim}
./bin/compareRuns detector runA runB 11a/13a zoom
            \end{verbatim}
            Where the detector is sk or nd5, runA and runB are the names of the runs you wish to compare (e.g. run4, or run1-4), 11a or 13a is the version of the tuning, and zoom is a boolean that determines the axis range. This will create commparison plots between the two runs, where any ratios are runB / runA. As an example:
            \begin{verbatim}
./bin/compareRuns nd5 run1 run4 13a true
            \end{verbatim}
            Which will create comparison plots of run4 and run1 at ND280 using the 13a release with the axis zoomed, where the ratios are run4 / run1. The output of the code should show all the plots that were created and where they were saved.

        \subsection{Slides!}
            The output of the flux release files must be presented to the Beam MC group for validation and approval. In general the nominal and tuned fluxes for all neutrino flavors along with the ratios of the current run to older runs are good to present. You can find old presentations of the flux release in previous Beam MC meetings.


    \newpage
    \begin{thebibliography}{9}
        \bibitem{flux_paper}
            K. Abe \emph{et al.} (T2K Collaboration),
            Phys. Rev. D. \textbf{87}, 012001 (2013), \\
            \url{https://doi.org/10.1103/PhysRevD.87.012001}

        \bibitem{TN38}
            T2K-TN-038 "Neutrino Flux Prediction" \\
            \url{http://www.t2k.org/docs/technotes/038/flux_prediction_v2}

        \bibitem{TN39}
            T2K-TN-039 "Neutrino Flux Uncertainty for the 10a data analysis" \\
            \url{http://www.t2k.org/docs/technotes/039/flux_uncertainty_v2}

        \bibitem{TN99}
            T2K-TN-099 "Flux Prediction and Uncertainties for the 2012a Oscillation Analysis" \\
            \url{http://www.t2k.org/docs/technotes/099/Beaminput12a.pdf}

        \bibitem{TN217}
            T2K-TN-217 "Flux Tuning and Uncertainty Updates for the 13a Flux" \\
            \url{http://www.t2k.org/docs/technotes/217/statistical-update-for-run-7c-sk-flux-tuning-1}

        \bibitem{TN264}
            T2K-TN-264 "Flux Release Summary" \\
            \url{https://www.t2k.org/docs/technotes/264/v3p0}

        \bibitem{geant}
            Geant3 Manual \\
            \url{http://hep.fi.infn.it/geant.pdf}

        \bibitem{geant_phys}
            Geant3 Physics Routines \\
            \url{http://www.star.bnl.gov/public/comp/simu/gstar/Manual/manual1.html}

        \bibitem{fluka}
            FLUKA Physics Routines \\
            \url{http://www.fluka.org/fluka.php?id=man_onl&sub=99}

    \end{thebibliography}

    \newpage
    \appendix
    \section{Possible Errors}

        \subsection{Internal Compilier Error}

        If the user does not have permissions to read the INCLUDE directories, e.g. \verb!CERN_ROOT!, the compilation fails with the following error: \verb!cc1: internal compiler error: Aborted! Fix the permissions of either the user or the INCLUDE directory to fix the error.

        \subsection{FLUKA SIGFPE Error}

        FLUKA crashes with a \verb!SIGFPE: Floating-point exception! error when generating events. To fix, add the following lines to \verb!hadpro/fluka_hadint/src/utils.f! just after the subroutine definition and before the variable declarations, then recompile.
        \begin{verbatim}
INCLUDE '(DBLPRC)'
INCLUDE '(DIMPAR)'
INCLUDE '(IOUNIT)'
        \end{verbatim}

        \subsection{Compilation Error: redefining getline()}
        
        Older versions of the code will fail when it tries to compile \verb!/libnubeam/clib.c! complaining about redefining the \verb!getline()! function. The work around for this is to compile \verb!clib.c! separately with a different flag. After compilation fails at \verb!clib.c! run the following commands (the gcc command is entered as one line):
        \begin{verbatim}
cd libnubeam
gcc -I/home/t2k/mhartz/cern/jbuild/2005/include 
-I/home/t2k/mhartz/cern/jbuild/2005/include/geant321 
-std=c89 -O2 -Wall -DCERNLIB_TYPE -Dextname -Dlynx -c clib.c
        \end{verbatim}

        You will get a number of warnings when compiling \verb!clib.c! but no errors should occur. Then you can run \verb!make! from the the \verb!jnubeam/! directory like you did previously to finish the compilation.

        \subsection{Jnubeam Jobs being cancelled}

        Another potential issue is Jnubeam producing too much text output when running. This may be different depending on what other changes people have made to the code, but some versions of Jnubeam need the following lines commented out to prevent jobs from being cancelled due to file sizes. In \verb!jnubeam/nubeam/gsim_find_gmother.F! comment out the following lines (line numbers may be approximate):
        \begin{verbatim}
Line 135: write(*,*) 'store at tgt ',idExitSK,' z: ',posExitSK(3)
Line 136: write(*,*) 'nd :',idExitFD,', ', posExitFD(3)
        \end{verbatim}

    \setlength{\parindent}{0pt}
    \section{Overview of Jnubeam Code}
    All the Geant3 code – written in Fortran77 – is stored in the nubeam/ repository. Let's quickly review the main files: \\

    \noindent \textbf{Any files containing a name of the beamline element}: Geant3-based geometrical description of the element. Better not touch it unless you know what you are doing. \\ \\
    \textbf{FlukaTreeReader}: Reads informations from the input Fluka file. \\ \\
    \textbf{gukine}: Gets target-exiting particles from Fluka and stores it in the Geant3 structures (ID, momentum, position, history up to the primary proton). If the interactions in the target is not generated by Fluka, this macro generates the primary interaction. \\ \\
    \textbf{gustep}: Propagates particles in the geometry. Special treatments are applied to muons (Geant 3 ID is 5 ($\mu^{+}$) and 6 ($\mu^{-}$) \cite{geant}) and neutrinos (in Geant3 convention, ID of a neutrino is 4 \cite{geant} – neutrinos are unflavored. Don't worry we fixed it in jnubeam.) \\ \\
    \textbf{neut gen}: Called every time a neutrino is produced. Computes relativistic kinematical variables for the neutrino, and then call NeutGenFD and NeutGenSK. \\ \\
    \textbf{NeutGenFD}: If we let Geant3 generate our neutrinos with proper kinematics, we would need huge statistics before having enough material to work on. Hence, every neutrino produced is now forced to go to near and far detectors and a kinematical weighting factor is computed (NB: Previously we were using an other method were every decay producing a neutrino was repeated 1000 times, no kinematical weights were applied). The neutrino are directed towards near detectors, the position in the transverse plane is chosen randomly. \\ \\
    \textbf{NeutGenSK}: Same as NeutGenFD, except that the direction to SK is fixed (SK is considered as point-like). \\ \\
    \textbf{fillFD, fillSK}: Implementation of the old method for recording more neutrinos (1000 decays).

    \section{Jnubeam Cardfile Settings}

    Here is an explanation of most of the parameters in the Jnubeam cardfile: \\

    \textbf{TRIG} Number of triggers you want to simulate. Beware that if you use an input file, the TRIG value must be equal or lower than the number of simulated events. \\
    
    \textbf{TILT} Downward angle of the target. User must be aware that a $2.5^{\circ}$-axis angle means a TILT value of 3.637. \\
    
    \textbf{PMOM} Proton momentum in GeV/c. Standard value is 30.9238. \\
    
    \textbf{PINT} Sets how the primary interactions are simulated. 0 and 1 values means that GEANT will take care of everything (beam is generated upstream the baffle/target respectively). A 2 value means that an input text file will be used. A 5 value means that Fluka files will be used (recommended). Beware that you will have to set the environment variable FLUKAROOT\_INPUT to the path of the input files before running the code. \\
    
    \textbf{TPOT} Total number of POT for neutrino flux estimation. Current value is 1.0e21. \\

    \textbf{BPOS} Proton beam parameters: x, y, [cm], dx/dz, dy/dz. Not used if you use input files. \\

    \textbf{BRMS} RMS of the beam radial size. \\
    
    \textbf{EMIT} Beam emittance parameters $\epsilon_{x}$, $\epsilon_{y}$ in units of pi.mm.mrad \\
    
    \textbf{ALPH} Beam twiss parameters $\alpha_{x}$ and $\alpha_{y}$. \\
    
    \textbf{HCUR} Horns 1, 2, and 3 current settings in kA. \\

    \textbf{BFPS, TRPS, H1PS, H2PS, and H3PS} Alignment of (respectively) the baffle, target, and horns. Parameters are: X(cm), Y(cm), S(cm), HTILT(deg) and VTILT(deg). \\
    
    \textbf{SUPK} SuperK parameters: distance (km) downward angle (deg) and nothern angle (deg). Based on the latest survey (May 2014) values are: 295.3368, 1.260 and 0.795. \\
    
    \textbf{NFRD} Number of near detectors you want to simulate. Near detectors and corresponding values are: 2km detector (1), proton module (2), INGRID horizontal module (3), INGRID vertical module (4), ND280 off-axis cargo (5), ND280 off-axis magnet (6), wall neutrino study (7), INGRID off-axis module 14 (8), INGRID off-axis module 15 (9),  MIZUCHE (10), INGRID individual modules (11, 12). If you set NFRD to a value X, all the near detectors up to X will be simulated. \\
    
    \textbf{XFRD, YFRD, ZFRD} X, Y, Z coordinate in NEUT (jnubeam mother volume) in m. You need to put as many parameters as NFRD, parameter X corresponds to near detector X. If you want to mask a detector, put a negative ZFRD value. \\

    \textbf{HSIZ, VSIZ} Horizontal and vertical half sizes of the detectors in meters. \\
    
    \textbf{FCOM} Geometry setting. 0: Full setup, 1: Apr-May 2009 commissioning run setup. \\
    
    \textbf{FSKI, FFRD} Store (respectively) SK and ND informations in normal output (1) or verbose output (2). Beware, for the latter, the output is 3 times larger. \\
    
    \textbf{FDFL} ND neutrino algorithm. Old routine (1000 decays) is 0, new routine is 1, both is 2. \\
    
    \textbf{LER*} If set to 1, low energy events (below 4 GeV) will be pre-scaled (strongly reduces the output size). Flavor code: LERM is $\nu_{\mu}$, LERE is $\nu_{e}$, LERB is $\overline{\nu}_{\mu}$ and LERI is $\overline{\nu}_{e}$. \\

    \textbf{EDEP} If set to 1, energy deposit in the target and decay volume will be stored. \\
    \textbf{FTES} If set to 1, test-plane informations will be stored. \\
    \textbf{FMUM} If set to 1, MUMON information will be stored. \\
    \textbf{VIEW} View control. (0 0) means no view control. \\
    \textbf{VCNT, VSCL} View settings. \\
    \textbf{TVIN} If set to 1, test volume will be seen. \\

    \textbf{ARND} Sets the way the random numbers are generated. Recommendation is to set this parameter to 4 so the seed is initialized by the environment variable NUBEAM\_RNDM\_ISEQ. \\
    
    \textbf{ANNI, BREM, COMP, DRAY, HADR, LOSS, MUNU, PAIR, PHOT} Controls the physics. For neutrino flux simulation, one should put these values (respectively) 2, 2, 2, 0, 6, 2, 2, 2, 2. For MUMON simulation: 1, 1, 1, 1, 6, 1, 1, 1, 1. For what these values corresponds to, see \cite{geant_phys}. \\
    
    \textbf{CUTS} Cut-off kinetic enery in GeV for gamma, electron, neutral hadrons, hadrons, muons. Original values are: 1.0 1.0 0.1 0.1 0.1 \\
    \textbf{CUBD, CUDV} Cut-off kinetic energy in dump and decay volume respectively in GeV.

    \section{Jnubeam Output Description}

    In the ROOT files produced by Jnubeam you have several TTrees: \\
    
    \textbf{h1000} : Parameters from the nubeam.card mainly relative to the beam. \\
    \textbf{h3000} : Parameters from the nubeam.card relative to the near detectors description. \\
    \textbf{h3001} : Near detectors variables if you have chosen the 1000 decays fill algorithm. \\
    \textbf{h3002} : Near detectors variable id you have chosen the new fill algorithm. \\
    \textbf{h2000} : Far detector variable. \\

    Variables for the near and far detectors are essientially the same: \\
    
    \textbf{Enu} : True neutrino energy. \\

    \textbf{ppid} : Geant3-based particle ID \cite{geant} of the parent of the neutrino. \\

    \textbf{mode} : Decay mode of the parent of the neutrino (also allows you to distinguish between different neutrino flavors). \\

    \textbf{ppi} : Total momentum of the parent. \\

    \textbf{xpi(3)} : Decay vertex of the parent in the global coordinate. \\

    \textbf{npi(3)} : Directional vector of the parent at decay point in global coordinate. \\

    \textbf{cospibm} : Cosine of the angle between the parent direction (at decay) and the beam direction. \\

    \textbf{norm} : Weight factor to be applied in order to get the flux in units of $\mathrm{flux}/\mathrm{cm}^{2}/1\times10^{21}$ POT. \\

    \textbf{ppi0, xpi0(3), npi0(3), cospi0bm} : Same as previously for the production point of the parent. \\

    \textbf{gipart, gpos(3), gvec(3), gamom0} : Primary particle ID, position, direction and momentum at starting point. \\

    \textbf{ng} : Number of generation of the neutrino. Primary particle is the number one. ng = 2 means that the parent of the neutrino was produced by the primary interaction. Maximum value stored is 12. Same purpose as \verb!nvtx0! without counting elastic scatterings. \\

    \textbf{gvx(ng), gvy(ng), gvz(ng), gpx(ng), gpy(ng), gpz(ng), gpid(ng), gcosbm(ng)} : Information relative to the ancestors. \\

    \textbf{gmec(ng)} : Production mechanism code of the ancestors. For the Fluka part, see \cite{fluka} for the convention. \\

    \textbf{gmat(ng)} : Material in which the ancestor has been created. 14 is the graphite (target). 5, 45, 70, 71 and 72 correspond to aluminum in the horns. \\
        
    Specific variables for the near detectors: \\

    \textbf{idfd} : Near detector ID number (see description of the nubeam.card NFRD parameter). \\

    \textbf{rnu, xnu, ynu} : Distance and position of the neutrino at the entrance of the detector in detector coordinates. \\

    \textbf{nnu(3)} : Neutrino direction in global coordinates. \\
    
    For example, if one wants the $\nu_{\mu}$ flux expected in ND280 cargo, here is the command to be typed in ROOT: \\

    \verb!h3002->Draw("Enu", "norm*(idfd==5 && mode >= 11 && mode < 16)")!

    \section{Installing (actual) FLUKA}

    The previous section on installing FLUKA really means installing the T2K fortran code that uses FLUKA routines, not installing the FLUKA Monte Carlo itself. This section is for actually installing the FLUKA Monte Carlo program when it inevitably gives you a message about the version of FLUKA being obsolete and unable to run. The FLUKA website where the latest version can be downloaded and various bits of documentation can be found here \url{http://www.fluka.org/fluka.php}. You need an user account to download FLUKA which is a pretty simple process and they will email you a FUID which will be used to download FLUKA. \\
    
    Download the tar archive for the 64-bit version of FLUKA and transfer it to the machine where you want to install it. Create a new directory and extract the tar file to that directory. FLUKA now requires gfortan to compile which on the KEKCC can be loaded with gcc/485 or higher, and requires a newer version of binutils, which can be found here: \verb!/group/t2k/beam/mc/beamMC/usr/bin! and added to your PATH. Two environment variables need to be specified before building FLUKA, set FLUFOR=gfortran and FLUPRO=/path/to/fluka (the directory where you extracted FLUKA). Run \verb!make! inside the directory where you extracted FLUKA and it should successfully build. The build instructions can be found in the README file that comes with FLUKA. Below are some sample instructions for installing FLUKA on the KEKCC. \\
    
    \begin{verbatim}
mkdir fluka2011.2c6
cd fluka2011.2c6
tar -zxvf fluka2011.2c-linux-gfor64bitAA.tar.gz
module load gcc/485
export PATH=/group/t2k/beam/mc/beamMC/usr/bin:$PATH
export FLUFOR=gfortran
export FLUPRO=/path/to/fluka2011.2c6
make
    \end{verbatim}

\end{document}
